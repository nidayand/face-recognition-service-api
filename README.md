# Face Recognition Service API 
The container contains of two parts: Learning and Recognition. The main purpose for the container is to service as a lightweight face recognition where an evaluation is performed through a simple POST request with the images to be evaluated. It also contains a lightweight interface to evaluate and retrain from the defined image set.

## Quick start
The git repository contains a sample set to quickly test the application and the following instructions are using the example library of images. Read further down to get more information on how to define the container.
1. Download the repository to get some sample images
```bash
$ git clone https://gitlab.com/nidayand/face-recognition-service-api.git
```
2. Create the Docker container
```bash
$ docker run -d -p 8080:8080 --name facerecog -v "$(pwd)/face-recognition-service-api/examples/dataset:/app/dataset" -e "TOLERANCE=0.6" nidayand/facerecognition
```
2. Goto http://localhost:8080
3. Upload a test image from **./face-recognition-service-api/examples/testset**

## Recognition
- Use the web form to upload the images for analysis. The response will be listed on the page with the detected faces. You can also use the camera on your phone to select an item to be uploaded
- Use a POST request to host:port/upload and the response will be a simple JSON array.  If there are no hits, the array will be empty
  ```console
  $ curl -F "file=@example_01.png" localhost:8080/upload

  {"items": ["ian_malcolm"]}
  ```
- If the service detects faces but cannot match them, they will be placed in an unrecognized folder to be used for re-learning the model

## Learning
- For initial learning, create folders in **dataset** for each of the persons that you want to evaluate against. Keep also one that must be named **unknown** where negative faces are to be put. Spelling in lower case is important. For the pictures, use a smaller size as the learning process is quite compute heavy and it depends a bit on the capacity of your server how large you can go. Make sure that the images only covers the face of the person to be detected
- Note that the naming of the image files doesn't really matter. The numbering below is just for illustration purposes
```
├── dataset
|   ├── unknown
|   |   ├── unknown1.jpg
|   |   ├── unknown2.jpg
|   |   └──unknown3.jpg
|   ├── steven
|   |   ├── steven1.jpg
|   |   ├── steven2.jpg
|   |   └──steven3.jpg
|   ├── jenny
|   |   ├── jenny.jpg
|   |   ├── jess.jpg
|   |   └── 0.jpg
|   ├── ...
```
- When the classes (folders) have been defined and at least 20 images are in each folder go to the root address and click on **Retrain** to create the pattern file that new images will be compared against
- When you run an evaluation against the model any unknown hits of faces will be put in a seperate folder for classification and you will be asked to move the file into one of the folders (in dataset) or delete it and it will later be used for re-training

## Docker
If you want to keep the dataset outside the container, map the **/app/dataset** folder. It is a good idea as it is easier to add and maintain the initial set of images that the model will learn from.

- **TOLERANCE** - environment variable to set a more strict matching. Read more under method **face_recognition.api.compare_faces** at https://face-recognition.readthedocs.io/en/latest/face_recognition.html. Note that a lower value is more strict. Default is 0.6
- **/app/dataset** - folder where learning images should be kept
- **/app/images** - folder for temporarily store uploaded images
- **/app/result** - folder for storing result images from a web form upload. If you use the POST API directly the images will not be kept (see curl example above)
- **/app/unrecognized** - folder for images that did not contain a match and can later be used for enhace the learning model
- **/app/templates** - folder for page templates

### Docker run command

```bash
$ docker run -d -p 8080:8080 --name facerecog -v "$(pwd)/myfolder/dataset:/app/dataset" -e "TOLERANCE=0.6" nidayand/facerecogntion
```

### Simple docker-compose.yml:
```yaml
version: '3'

services:
  facerecog:
    image: nidayand/facerecognition
    environment:
      - TOLERANCE=0.6
    volumes:
      - "./myfolder/dataset:/app/dataset"
    ports:
      - "8080:8080"
```


## Credits
The Docker container is based on the great tutorial from Adrian Rosebrock @pyimagesearch.com
https://www.pyimagesearch.com/2018/06/25/raspberry-pi-face-recognition/