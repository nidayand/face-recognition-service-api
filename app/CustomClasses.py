# import the necessary packages
import face_recognition
import argparse
import imutils
import pickle
import time
import cv2
from imutils import paths
import logging
import uuid

class AnalyzePersonImage:
    def __init__(self):
        self.loadEncodings()

    def loadEncodings(self):
        # load the known faces and embeddings along with OpenCV's Haar
        # cascade for face detection
        logging.info("loading encodings and classifier...")
        self.data = pickle.loads(open("encodings.pickle", "rb").read())
        self.detector = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")

    def reloadEncodings(self):
        pass

    def retrainEncodings(self):
        pass

    def getPersonInImage(self, imagePath, createBoxes=False, saveResult='./', tolerance=0.6):
        # grab the frame from the threaded video stream and resize it
        # to 500px (to speedup processing)
        frame = cv2.imread(imagePath)
        frame = imutils.resize(frame, width=500)

        # convert the input frame from (1) BGR to grayscale (for face
        # detection) and (2) from BGR to RGB (for face recognition)
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        rgb = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

        # detect faces in the grayscale frame
        rects = self.detector.detectMultiScale(gray, scaleFactor=1.1, 
            minNeighbors=5, minSize=(30, 30),
            flags=cv2.CASCADE_SCALE_IMAGE)

        # OpenCV returns bounding box coordinates in (x, y, w, h) order
        # but we need them in (top, right, bottom, left) order, so we
        # need to do a bit of reordering
        boxes = [(y, x + w, y + h, x) for (x, y, w, h) in rects]

        # compute the facial embeddings for each face bounding box
        encodings = face_recognition.face_encodings(rgb, boxes)
        names = []

        # loop over the facial embeddings
        foundUnknown = False
        for encoding in encodings:
            # attempt to match each face in the input image to our known
            # encodings

            #Tolerance 0.6 https://face-recognition.readthedocs.io/en/latest/face_recognition.html
            matches = face_recognition.compare_faces(self.data["encodings"],
                encoding, tolerance)
            name = "unknown"

            # check to see if we have found a match
            if True in matches:
                # find the indexes of all matched faces then initialize a
                # dictionary to count the total number of times each face
                # was matched
                matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                counts = {}

                # loop over the matched indexes and maintain a count for
                # each recognized face face
                for i in matchedIdxs:
                    name = self.data["names"][i]
                    counts[name] = counts.get(name, 0) + 1

                # determine the recognized face with the largest number
                # of votes (note: in the event of an unlikely tie Python
                # will select first entry in the dictionary)
                name = max(counts, key=counts.get)

                # Store for retraining
                if name == "unknown":
                    foundUnknown = True
            
            # update the list of names
            if name != "unknown":
                logging.debug("File: "+imagePath)
                logging.debug("Found: "+name)
            
            names.append(name)
            

        logging.debug(names)

        # loop over the recognized faces and save the image
        # if createBoxes is set to True
        if createBoxes:
            foundBox = False
            # Copy frame for unknown
            orig_frame = frame.copy()
            for ((top, right, bottom, left), name) in zip(boxes, names):
                # Skip if name = unknown
                if name == "unknown":
                    # Increase size by 100%
                    y = int(top - (bottom - top)/2)
                    ym = int(bottom + (bottom - top)/2)
                    x = int(left - (right - left)/2)
                    xm = int(right + (right - left)/2)

                    # Only save if x and y > 0
                    if y>0 and x>0:
                        logging.debug("Cropping to "+str(y)+"/"+str(ym)+ ":"+ str(x)+"/"+str(xm))
                        crop_frame = orig_frame.copy()[y:ym, x:xm]
                        cv2.imwrite("unrecognized/"+str(uuid.uuid4())+".jpg", crop_frame,  [int(cv2.IMWRITE_JPEG_QUALITY), 85])               
                else:
                    foundBox = True
                    # draw the predicted face name on the image
                    cv2.rectangle(frame, (left, top), (right, bottom),
                        (0, 255, 0), 2)
                    y = top - 15 if top - 15 > 15 else top + 15
                    cv2.putText(frame, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                        0.75, (0, 255, 0), 2)

            # show the output image
            if foundBox:
                cv2.imwrite(saveResult + str(uuid.uuid4())+".jpg", frame,  [int(cv2.IMWRITE_JPEG_QUALITY), 85])
            
        return names
