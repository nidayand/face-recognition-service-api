import os
from flask import Flask, render_template, request, send_from_directory, redirect
from CustomClasses import AnalyzePersonImage
import logging
import json
from imutils import paths
import face_recognition
import argparse
import pickle
import cv2
import os
import shutil

# Get environment variable for tolerance
tolerance = float(os.getenv('TOLERANCE', '0.6'))
debug = int(os.getenv('DEBUG', '30'))

app = Flask(__name__)

APP_ROOT = os.path.dirname(os.path.abspath(__file__))

logging.basicConfig(level=debug)

# Initialize analyzing class
personImage = AnalyzePersonImage()

logging.info("Tolerance level is set to: "+str(tolerance))
logging.info("Debug level is set to: "+str(debug))

@app.route("/")
def index():
    # Check if there are unrecognized images
    image_names = []
    for name in os.listdir('./unrecognized'):
        # List all jpg with size > 0
        if name.endswith(".jpg") and os.path.getsize("./unrecognized/"+name)>0:
            image_names.append(name)

    # Result names
    result_names = []
    for name in os.listdir('./result'):
        if name.endswith(".jpg"):
            result_names.append(name)

    names = []
    for name in os.listdir("./dataset"):
        if not name.startswith("."):
            names.append(name)
    return render_template("upload.html", image_names=image_names, persons=names, show_unrecognized=(len(image_names)>0), show_result=(len(result_names)>0), result_names=result_names, tolerance=tolerance)

@app.route('/upload/<directory>/<filename>')
def send_image(directory, filename):
    return send_from_directory(directory, filename)

@app.route('/edit/move', methods=["GET"])
def move_image():
    image = request.args.get('image', '')
    folder = request.args.get('folder', '')
    delete = request.args.get('delete', 'false')
    url = request.args.get('url', '/')

    # Check if image is to be deleted
    if delete in ['true','True','1','yes','YES','TRUE']:
        os.remove('./unrecognized/'+image)
    else:
        # Move to folder
        shutil.move('./unrecognized/'+image, './dataset/'+folder+'/'+image)

    return redirect(url)

@app.route('/edit/tolerance', methods=["GET"])
def set_tolerance():
    global tolerance
    # Clear unrecognized and result folder if value is changed
    unrecognizedFolder = "unrecognized/"
    for name in os.listdir(unrecognizedFolder):
        if name != ".keep":
            os.remove(unrecognizedFolder+name)
    resultFolder = "result/"
    for name in os.listdir(resultFolder):
        if name != ".keep":
            os.remove(resultFolder+name)

    # Get tolerance value parameter
    tolerance = float(request.args.get('value', str(tolerance)))

    # Return ok JSON
    response = app.response_class(
        response=json.dumps({ "status": 1 }),
        status=200,
        mimetype='application/json'
    )
    check_persons(True, True)
    return response    

@app.route("/status", methods=["GET"])
def get_status():
    global trainImage, trainStatus, trainTotal
    response = app.response_class(
        response=json.dumps({ "running": trainStatus, "activeImage":trainImage, "totalImages": trainTotal, "percentage": int(trainImage/trainTotal*100)}),
        status=200,
        mimetype='application/json'
    )
    return response    

trainImage = 0
trainTotal = 100
trainStatus = False
@app.route('/retrain', methods=["POST"])
def retrain():
    global personImage

    # grab the paths to the input images in our dataset
    logging.info("Quantifying faces...")
    imagePaths = list(paths.list_images("dataset"))

    # initialize the list of known encodings and known names
    knownEncodings = []
    knownNames = []

    # loop over the image paths
    global trainImage, trainStatus, trainTotal
    trainStatus = True
    try:
        trainTotal = len(imagePaths)

        for (i, imagePath) in enumerate(imagePaths):
            # extract the person name from the image path
            logging.info("processing image {}/{}".format(i + 1,
                len(imagePaths)))
            name = imagePath.split(os.path.sep)[-2]
            trainImage = i

            # load the input image and convert it from RGB (OpenCV ordering)
            # to dlib ordering (RGB)
            image = cv2.imread(imagePath)
            rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            # detect the (x, y)-coordinates of the bounding boxes
            # corresponding to each face in the input image
            boxes = face_recognition.face_locations(rgb,
                model="hog")

            # compute the facial embedding for the face
            encodings = face_recognition.face_encodings(rgb, boxes)

            # loop over the encodings
            for encoding in encodings:
                # add each encoding + name to our set of known names and
                # encodings
                knownEncodings.append(encoding)
                knownNames.append(name)

        data = {"encodings": knownEncodings, "names": knownNames}
        f = open("encodings.pickle", "wb")
        f.write(pickle.dumps(data))
        f.close()
    except:
        logging.error("Failed to analyze images")
    finally:
        trainStatus = False
        logging.info("Reloading encodings")
        personImage = AnalyzePersonImage()
    return "ok"

@app.route("/upload", methods=["POST"])
def upload():
    global personImage, tolerance

    target = os.path.join(APP_ROOT, "images/")
    resultFolder = os.path.join(APP_ROOT, "result/")

    # Empty result folder
    if not os.path.isdir(resultFolder):
        logging.debug("Creating result folder images")    
    for name in os.listdir(resultFolder):
        if name != ".keep":
            os.remove(resultFolder+name)

    if not os.path.isdir(target):
        logging.debug("Creating upload folder images")
        os.mkdir(target)
    for name in os.listdir(target):
        if name != ".keep":
            os.remove(target+name)

    # Clear unrecognized and result folder if value is changed
    unrecognizedFolder = "unrecognized/"
    for name in os.listdir(unrecognizedFolder):
        if name != ".keep":
            os.remove(unrecognizedFolder+name)

    # Default will not save the result. If the web form
    # is used the results will be saved
    saveResult=False
    try:
        if request.form.get("saveResult"):
            saveResult=True
    except KeyError as identifier:
        pass
    for file in request.files.getlist("file"):
        destination = "/".join([target, file.filename])

        #Store file
        file.save(destination)
    
    # Call check for personse
    if not saveResult:
        response = app.response_class(
            response=json.dumps({ "items": check_persons(False)}),
            status=200,
            mimetype='application/json'
        )
        return response
    else:
        check_persons(True)
        return redirect("/")

# Check files in the images folder for faces
# Redirect param is used when tolerance is changed and
#   method should only complete
def check_persons(saveResult=False, redirect=False):
    global tolerance
    target = os.path.join(APP_ROOT, "images/")
    resultFolder = os.path.join(APP_ROOT, "result/")

    # Store total list result
    totalList = []

    for name in os.listdir(target):
        # Skip .keep files
        if name == ".keep":
            continue

        image = "/".join([target, name])

        # Check if the file contains approved persons
        logging.debug("Starting visual recognition on file")
        logging.debug(image)
        logging.debug(resultFolder)


        hits = personImage.getPersonInImage(image, saveResult, resultFolder, tolerance)
        for n in hits:
            if n not in totalList and n != "unknown":
                logging.debug("Adding '"+n+"' to the list of persons")
                totalList.append(n)

        # Remove file after processing
        # logging.debug("Deleted uploaded file post processing")
        # os.remove(destination)
    if redirect:
        return

    logging.debug("Found the following persons: "+" ".join(str(x) for x in totalList))
    return totalList
    

if __name__ == "__main__":
    if debug == 10:
        logging.debug("***** Starting server in debug mode *****")
        app.run(port=8080, debug=True, host='0.0.0.0')
    else:
        app.run(port=8080, host='0.0.0.0')