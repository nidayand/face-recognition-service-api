FROM python:3.7.3-slim as face-build-env
MAINTAINER nidayand <pggithub@gothager.se>
ENV OPENCV_VERSION="4.1.0"
ENV DLIB_VERSION="19.17"

RUN apt-get update && \
        apt-get install -y \
        build-essential \
        cmake \
        git \
        wget \
        unzip \
        yasm \
        pkg-config \
        libopenblas-dev \
        liblapack-dev 

RUN pip install --upgrade pip
RUN pip install numpy

WORKDIR /
RUN wget -O opencv.zip https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip \
&& unzip opencv.zip && rm opencv.zip
RUN wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/${OPENCV_VERSION}.zip \
&& unzip opencv_contrib.zip && rm opencv_contrib.zip

RUN mv opencv-${OPENCV_VERSION} /opencv
RUN mv opencv_contrib-${OPENCV_VERSION} /opencv_contrib

RUN mkdir /opencv/cmake_binary \
&& cd /opencv/cmake_binary \
&& cmake -DBUILD_TIFF=ON \
  -D OPENCV_EXTRA_MODULES_PATH=/opencv_contrib/modules \
  -D BUILD_opencv_java=OFF \
  -D BUILD_NEW_PYTHON_SUPPORT=ON \
  -D WITH_OPENGL=ON \
  -D WITH_OPENMP=ON \
  -D WITH_OPENCL=ON \
  -D BUILD_DOCS=OFF \
  -D WITH_EIGEN=OFF \
  -D BUILD_TESTS=OFF \
  -D BUILD_PERF_TESTS=OFF \
  -D BUILD_EXAMPLES=OFF \
  -D WITH_GTK=ON \
  -D WITH_TBB=ON \
  -D WITH_V4L=OFF \
  -D WITH_QT=OFF \
  -D CMAKE_BUILD_TYPE=RELEASE \
  -D CMAKE_INSTALL_PREFIX=$(python3.7 -c "import sys; print(sys.prefix)") \
  -D PYTHON_EXECUTABLE=$(which python3.7) \
  -D PYTHON_INCLUDE_DIR=$(python3.7 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
  -D PYTHON_PACKAGES_PATH=$(python3.7 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") .. \
&& make install \
&& rm -r /opencv \
&& rm -r /opencv_contrib

# Compile dlib
RUN wget -O dlib.zip https://github.com/davisking/dlib/archive/v${DLIB_VERSION}.zip \
  && unzip dlib.zip \
  && rm dlib.zip \
  && cd dlib-* \
  && mkdir build \
  && cd build \
  && cmake .. -DUSE_AVX_INSTRUCTIONS=1 \
  && cmake --build . \
  && cd .. \
  && python setup.py install \
  && cd .. \
  && rm -rf dlib-*

FROM python:3.7.3-slim
RUN apt-get update && \
        apt-get install -y \
        libjpeg-dev \
        libpng-dev \
        libtiff-dev \
        libavcodec-dev \
        libavformat-dev \
        libswscale-dev \
        libv4l-dev \
        libopenblas-dev \
        liblapack-dev 

RUN pip install --upgrade pip
RUN pip install numpy

WORKDIR /
COPY --from=face-build-env /usr/local/bin /usr/local/bin
COPY --from=face-build-env /usr/local/lib /usr/local/lib
COPY --from=face-build-env /usr/local/include /usr/local/include

RUN pip install requests
RUN pip install Flask
RUN pip install face_recognition
RUN pip install imutils

# Clean up
RUN apt-get clean -y
RUN apt-get autoclean
RUN apt-get autoremove -y

# Set the working directory to /app
WORKDIR /app

ADD ./app /app

# Make port 80 available to the world outside this container
EXPOSE 8080

# Run app.py when the container launches
CMD ["python", "service.py"]